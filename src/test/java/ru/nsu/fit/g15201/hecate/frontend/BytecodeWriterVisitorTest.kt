package ru.nsu.fit.g15201.hecate.frontend

import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import ru.nsu.fit.g15201.hecate.antlr4.HecateLexer
import ru.nsu.fit.g15201.hecate.antlr4.HecateParser

internal class BytecodeWriterVisitorTest {
    private fun parseInput(input: String): String? {
        val charStream = CharStreams.fromString(input)
        val lexer = HecateLexer(charStream)
        val tokenStream = CommonTokenStream(lexer)
        val parser = HecateParser(tokenStream)
        val parseTree = parser.classDeclaration()
        val bytecode = BytecodeWriterVisitor()
        bytecode.visit(parseTree)
        return bytecode.getStringForTest()?.replace("\n", "")?.replace(" ", "")
    }

    @Test
    fun visitClassDeclaration() {
        val input = "class hello {}"
        val expected = "class hello {}".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }

    @Test
    fun visitMethodDeclaration() {
        val input = "class c {fun foo(): void {} \n fun goo(): void {}}"
        val expected = "class c{void foo() {} void goo(){} }".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }

    @Test
    fun visitFunctionValueParameters() {
        val input = "class c {fun foo(x: int, y:double): void {}}"
        val expected = "class c{void foo(int x, double y) {}}".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }

    @Test
    fun visitPrimary() {
        val input = "class c {\n" +
                "    fun foo(x: int, y:double): void {\n" +
                "        x = 8;\n" +
                "        y = x;\n" +
                "    }\n" +
                "}"
        val expected = "class c{void foo(int x, double y) {x = 8; y = x;}}".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }

    @Test
    fun visitExpression() {
        val input = "class c {\n" +
                "    fun foo(x: int, y:double): void {\n" +
                "        x = 8 + y++;\n" +
                "        y = ++x;\n" +
                "    }\n" +
                "}"
        val expected = "class c{void foo(int x, double y) {x = 8 + y++; y = ++x;}}".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }

    @Test
    fun visitStatement() {
        val input = "class c {\n" +
                "    fun foo(x: int, y:double): void {\n" +
                "        x = y;\n" +
                "        y = x;\n" +
                "        x = y;\n" +
                "    }\n" +
                "}"
        val expected = "class c{void foo(int x, double y) {x = y; y = x; x = y;}}".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }

    @Test
    fun visitElseStatement() {
        val input = "class hello {\n" +
                "    fun f(x : int, y : double) : void {\n" +
                "        var t : int = 88;\n" +
                "        var k : double = 109;\n" +
                "        while (n > 0) {}\n" +
                "    }\n" +
                "}"
        val expected = "class hello{void f(int x, double y) {int t = 88; double k = 109; while(n>0) {}}}".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }

    @Test
    fun visitReturnStatement() {
        val input = "class hello {\n" +
                "    fun f(x : int, y: double) : int {\n" +
                "        var t : int = 89;\n" +
                "        t += x * y;\n" +
                "        return t;\n" +
                "    }\n" +
                "}\n"
        val expected = "class hello{int f(int x, double y) {int t = 89; t += x * y; return t;}}".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }
    @Test
    fun visitPropertyDeclaration() {
        val input = "class c {\n" +
                "    fun f(x : int, y : double) : void {\n" +
                "        var t : int = 89;\n" +
                "    }\n" +
                "}"
        val expected = "class c{void f(int x, double y) {int t = 89;}}".replace(" ", "")
        assertEquals(parseInput(input), expected)
    }
}
