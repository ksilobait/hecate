parser grammar HecateParser;

options { tokenVocab = HecateLexer; }

importDeclaration
    : IMPORT qualifiedName ('.' '*')? ';'
;

classDeclaration
    : CLASS IDENTIFIER
    classBody
;

functionDeclaration
    : methodDeclaration
;

qualifiedName
    : IDENTIFIER ('.' IDENTIFIER)*
;

classBody
    : '{' classBodyDeclaration* '}'
;

classBodyDeclaration
    : ';'
    | memberDeclaration
;

memberDeclaration
    : methodDeclaration
    | propertyDeclaration
;

methodDeclaration
    : FUN
    simpleIdentifier
    functionValueParameters
    COLON type
    block
    ;

functionValueParameters
    : LPAREN (functionValueParameter (COMMA functionValueParameter)*)? RPAREN
;

functionValueParameter
    :  simpleIdentifier COLON type
;

type
    : IDENTIFIER
;

simpleIdentifier
    : IDENTIFIER
;

block
    : '{' blockStatement* '}'
;

blockStatement
    : propertyDeclaration ';'
    | statement
    ;

propertyDeclaration
    : (VAL | VAR)
    variableDeclaration
    (ASSIGN expression)
;

variableDeclaration
    : simpleIdentifier COLON type
;

statement
    : declaration
    | assignment
    | expression
    | IF parExpression block (elseStatement)?
    | WHILE parExpression block
    | returnStatement
    | SEMI
    | statementExpression=expression ';'
;

returnStatement
    : RETURN expression? ';';

elseStatement
    : ELSE block
;

declaration
    : propertyDeclaration
;

assignment
    : assignableExpression assignmentOperator expression
;

assignmentOperator
    : ASSIGN_IDENTIFIER
;

assignableExpression
    : IDENTIFIER
;

parExpression
    : '(' expression ')'
;

expression
    : primary
    | expression bop='.'
      ( IDENTIFIER
      | methodCall
//      | THIS
      )
    | methodCall
    //| '(' type ')' expression
    | expression postfix=('++' | '--')
    | prefix=('+'|'-'|'++'|'--') expression
    | prefix='!' expression
    | expression bop=('*'|'/'|'%') expression
    | expression bop=('+'|'-') expression
    | expression ('<' '<' | '>' '>' '>' | '>' '>') expression
    | expression bop=('<=' | '>=' | '>' | '<') expression
    | expression bop=IS type
    | expression bop=('==' | '!=') expression
    | expression bop='&&' expression
    | expression bop='||' expression
    | <assoc=right> expression
      bop=('=' | '+=' | '-=' | '*=' | '/=' | '%=')
      expression
;

methodCall
    : IDENTIFIER '(' expressionList? ')'
    | THIS '(' expressionList? ')'
;

expressionList
    : expression (',' expression)*
;

primary
    : '(' expression ')'
    | THIS
    | literal
    | IDENTIFIER
    | type '.' CLASS
;


literal
    : DECIMAL_LITERAL
    | FLOAT_LITERAL
    | CHAR_LITERAL
    | STRING_LITERAL
    | BOOL_LITERAL
    | NULL_LITERAL
;
