package ru.nsu.fit.g15201.hecate.frontend

import ru.nsu.fit.g15201.hecate.antlr4.HecateParser
import ru.nsu.fit.g15201.hecate.antlr4.HecateParserBaseVisitor
import ru.nsu.fit.g15201.hecate.backend.ClassBuilder

class BytecodeWriterVisitor : HecateParserBaseVisitor<Any?>() {
    private lateinit var classBuilder: ClassBuilder

    fun getStringForTest(): String? {
        return classBuilder.builder.toString()
    }

    override fun visitClassDeclaration(ctx: HecateParser.ClassDeclarationContext): Any? {
        classBuilder = ClassBuilder(ctx.IDENTIFIER().symbol.text)

        val result = visitChildren(ctx)

        classBuilder.build()
        return result
    }

    override fun visitMethodDeclaration(ctx: HecateParser.MethodDeclarationContext?): Any? {
        val name = ctx?.simpleIdentifier()?.IDENTIFIER()?.symbol?.text
        val returnType = ctx?.type()?.IDENTIFIER()?.symbol?.text
        //println(ctx?.block()) //TODO
        classBuilder.addFunction(name, returnType)
        val toReturn = super.visitMethodDeclaration(ctx)
    //    classBuilder.endMethod()
        return toReturn
    }

    override fun visitFunctionValueParameters(ctx: HecateParser.FunctionValueParametersContext?): Any? {
        classBuilder.addParameters(ctx?.functionValueParameter())
        return super.visitFunctionValueParameters(ctx)
    }

    override fun visitPrimary(ctx: HecateParser.PrimaryContext?): Any? {
        val value: String?
        if (!ctx?.IDENTIFIER()?.symbol?.text.isNullOrEmpty()) {
            value = ctx?.IDENTIFIER()?.symbol?.text
        } else if (!ctx?.literal()?.BOOL_LITERAL()?.text.isNullOrEmpty()) {
            value = ctx?.literal()?.BOOL_LITERAL()?.text
        } else if (!ctx?.literal()?.CHAR_LITERAL()?.text.isNullOrEmpty()) {
            value = ctx?.literal()?.CHAR_LITERAL()?.text
        } else if (!ctx?.literal()?.DECIMAL_LITERAL()?.text.isNullOrEmpty()) {
            value = ctx?.literal()?.DECIMAL_LITERAL()?.text
        } else if (!ctx?.literal()?.FLOAT_LITERAL()?.text.isNullOrEmpty()) {
            value = ctx?.literal()?.FLOAT_LITERAL()?.text
        } else if (!ctx?.literal()?.NULL_LITERAL()?.text.isNullOrEmpty()) {
            value = ctx?.literal()?.NULL_LITERAL()?.text
        } else if (!ctx?.literal()?.STRING_LITERAL()?.text.isNullOrEmpty()) {
            value = ctx?.literal()?.STRING_LITERAL()?.text
        } else {
            value = ""
        }
        classBuilder.addPrimary(value)
        return super.visitPrimary(ctx)
    }

    override fun visitExpression(ctx: HecateParser.ExpressionContext?): Any? {
        val bop = ctx?.bop?.text
        val prefix = ctx?.prefix?.text
        val postfix = ctx?.postfix?.text
        if (!bop.isNullOrEmpty()) {
            classBuilder.store(bop)
        } else if (!prefix.isNullOrEmpty()) {
            classBuilder.addPrimary(prefix)
        } else if (!postfix.isNullOrEmpty()) {
            classBuilder.store(postfix)
        }
        return super.visitExpression(ctx)
    }

    override fun visitStatement(ctx: HecateParser.StatementContext?): Any? {
        val semi = ctx?.SEMI()?.symbol?.text
        val whileWord = ctx?.WHILE()?.symbol?.text
        val ifWord = ctx?.IF()?.symbol?.text
        if (!semi.isNullOrEmpty()) {
            classBuilder.addPrimary("$semi\n")
        }
        if (!whileWord.isNullOrEmpty()) {
            classBuilder.addPrimary("$whileWord")
        }
        if (!ifWord.isNullOrEmpty()) {
            classBuilder.addPrimary("$ifWord")
        }
        return super.visitStatement(ctx)
    }

    override fun visitElseStatement(ctx: HecateParser.ElseStatementContext?): Any? {
        val elseWord = ctx?.ELSE()?.symbol?.text
        if (!elseWord.isNullOrEmpty()) {
            classBuilder.addPrimary("$elseWord")
        }
        return super.visitElseStatement(ctx)
    }

    override fun visitReturnStatement(ctx: HecateParser.ReturnStatementContext?): Any? {
        val returnWord = ctx?.RETURN()?.symbol?.text
        classBuilder.addPrimary("$returnWord")
        val toReturn = super.visitReturnStatement(ctx)
        classBuilder.addPrimary(";")
        return toReturn
    }

    override fun visitBlock(ctx: HecateParser.BlockContext?): Any? {
        classBuilder.addPrimary("{\n")
        val toReturn = super.visitBlock(ctx)
        classBuilder.addPrimary("\n}")
        return toReturn
    }

    override fun visitParExpression(ctx: HecateParser.ParExpressionContext?): Any? {
        classBuilder.addPrimary("(")
        val toReturn = super.visitParExpression(ctx)
        classBuilder.addPrimary(") ")
        return toReturn
    }

    override fun visitPropertyDeclaration(ctx: HecateParser.PropertyDeclarationContext?): Any? {
        val varname = ctx?.variableDeclaration()?.simpleIdentifier()?.IDENTIFIER()?.symbol?.text
        val vartype = ctx?.variableDeclaration()?.type()?.IDENTIFIER()?.symbol?.text
        classBuilder.addPrimary("$vartype $varname = ")
        val toReturn = super.visitPropertyDeclaration(ctx)
        classBuilder.addPrimary(";\n")
        return toReturn
    }
}
