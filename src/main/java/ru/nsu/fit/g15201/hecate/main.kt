package ru.nsu.fit.g15201.hecate

import org.mdkt.compiler.InMemoryJavaCompiler
import ru.nsu.fit.g15201.hecate.frontend.GrammarRecognizer
import java.io.File


fun main(args: Array<String>) {
    val recognizer = GrammarRecognizer(File("src.hecate"))
    val classBody = recognizer.generateClassFile()
}

fun returnClass(classBody: String?): Class<*>? {
    val inMemoryJavaCompiler = InMemoryJavaCompiler.newInstance()
    return inMemoryJavaCompiler.compile("hello", classBody)
}
