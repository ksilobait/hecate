package ru.nsu.fit.g15201.hecate.backend

import ru.nsu.fit.g15201.hecate.antlr4.HecateParser
import java.io.File
import java.lang.StringBuilder

class ClassBuilder(className: String) {
    internal val builder = StringBuilder("class $className {\n")
    private var stored : String? = null

    fun build() {
        builder.append("}")
        File("./temp/hello.java").writeText(builder.toString())
    }

    fun addFunction(name: String?, returnType: String?) {
        builder.append("$returnType $name(")
    }

    fun addParameters(args: List<HecateParser.FunctionValueParameterContext>?) {
        for (i in 0 until args?.size!! - 1) {
            val name = args[i].simpleIdentifier().IDENTIFIER().symbol.text
            val type = args[i].type().IDENTIFIER().symbol.text
            builder.append("$type $name, ")
        }
        if (args.isEmpty()) {
            builder.append(") ")
        } else {
            val name = args[args.size - 1].simpleIdentifier().IDENTIFIER().symbol.text
            val type = args[args.size - 1].type().IDENTIFIER().symbol.text
            builder.append("$type $name) ")
        }
    }

    fun addPrimary(value: String?) {
        builder.append("$value ")
        if (!stored.isNullOrEmpty()) {
            builder.append("$stored ")
        }
        stored = null
    }

    fun store(text: String?) {
        stored = text
    }

    fun endMethod() {
        builder.append("}\n")
    }
}
