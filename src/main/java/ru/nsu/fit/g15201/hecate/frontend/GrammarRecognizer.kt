package ru.nsu.fit.g15201.hecate.frontend

import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import ru.nsu.fit.g15201.hecate.antlr4.HecateLexer
import ru.nsu.fit.g15201.hecate.antlr4.HecateParser
import java.io.File

class GrammarRecognizer(inputFile: File) {
    private val parser: HecateParser

    init {
        val charStream = CharStreams.fromFileName(inputFile.name)
        val lexer = HecateLexer(charStream)
        val tokenStream = CommonTokenStream(lexer)
        parser = HecateParser(tokenStream)
    }

    fun generateClassFile(): String? {
        val parseTree = parser.classDeclaration()
        val visitor = BytecodeWriterVisitor()
        visitor.visit(parseTree)
        return visitor.getStringForTest()
    }
}
